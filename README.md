### Pre-requirements

[Docker](https://docs.docker.com/get-docker/)

### Launch

1. `docker pull e333/dont-do-obscure-sabotage`
2. `docker run --network="host" e333/dont-do-obscure-sabotage`

### Environment config

Located in .env file.

- **THREADS_AMOUNT** - `40` by default. Amount of sites to ddos in parallel.
- **REQUESTS_PER_THREAD** - `100` by default. Amount of parallel requests per thread.
- **TIMEOUT_SECS** - `20` seconds by default. Amount of seconds to wait until ddos request finishes.
- **PROXY_VERIFIER** - defaults to `https://yandex.ru`. The site which should be reachable via proxy to consider proxy
  as valid.

### Txt config

- _enemy.txt_ - a list of sites to ddos.
- _proxy.txt_ - a static list of verified proxies.
- _language.txt_ - a list of possible values for ACCEPT-LANGUAGE header.
- _referrer.txt_ - a list of possible values for REFERER header.
- _user_agent.txt_ - a list of possible values for USER-AGENT header.

### Load logic

1. Get THREADS_AMOUNT of sites from _enemy.txt_.
2. Get all possible proxies from _proxy.txt_ + dynamic sources of proxies.
3. For each site make a request through each proxy with max waiting time of TIMEOUT_SECS.
4. If request is successful - extract all local urls from response & make requests to them too.
5. Go to step 1.

Notes for each step:

1. Every site will be loaded in a separate thread. The start index of sites slice is based on current minute. This is
   done to coordinate the load from multiple servers without coordinator to simultaneously create load to the same list
   of sites from all servers which use this script.
2. Proxies are pulled and validate in parallel with main logic. That's why script starts with 0 proxies and increases
   amount of proxies over time. All proxies are tested for functionality by using them to access PROXY_VERIFIER.
3. All requests are executed asynchronously(~in parallel).
4. This peace of logic is executed only if site returns 200. In this case script finds all <a\> tags with hrefs to
   itself.
5. Because of static timeout(TIMEOUT_SECS) the rps/rpm/load can be controlled via THREADS_AMOUNT & TIMEOUT_SECS env
   variables.

### Making changes in config

If you changed env variables, you just need to restart the script. If you changed txt config, then you need to rebuild &
restart the script.