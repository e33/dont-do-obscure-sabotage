use std::env;
use std::str::FromStr;

pub fn config_get_timeout_secs() -> u64 {
    get_env_variable("TIMEOUT_SECS", 20)
}

pub fn config_get_proxy_verifier() -> String {
    get_env_variable("PROXY_VERIFIER", "https://yandex.ru".to_owned())
}

pub fn config_get_threads_amount() -> usize {
    get_env_variable("THREADS_AMOUNT", 40)
}

pub fn config_get_request_per_thread() -> usize {
    get_env_variable("REQUESTS_PER_THREAD", 100)
}

fn get_env_variable<T>(variable: &str, default: T) -> T
    where T: FromStr
{
    if let Ok(v) = env::var(variable) {
        v.parse::<T>().map_or(default, |e| e)
    } else {
        default
    }
}