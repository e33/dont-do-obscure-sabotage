use crossbeam_channel::{Receiver, Sender};

use crate::GuiEvent;

pub struct EnemyThreadConnection {
    in_: Receiver<&'static str>,
    out: Sender<EnemyThreadOutEvent>,
    gui_tx: &'static Sender<GuiEvent>,
}

impl EnemyThreadConnection {
    pub fn new(in_: Receiver<&'static str>, out: Sender<EnemyThreadOutEvent>, gui_tx: &'static Sender<GuiEvent>) -> EnemyThreadConnection {
        EnemyThreadConnection {
            in_,
            out,
            gui_tx,
        }
    }

    pub fn get_new_enemy(&mut self) -> Option<&'static str> {
        if self.out.send(EnemyThreadOutEvent::GetNewEnemy).is_err() {
            return None;
        }

        self.in_.recv().ok()
    }

    pub fn request_done(&self, site: &'static str, statuses: Vec<u16>) {
        self.gui_tx.send(GuiEvent::RequestDone(site, statuses))
            .expect("Cannot send request done event");
    }
}

pub enum EnemyThreadOutEvent {
    GetNewEnemy,
}