use std::collections::HashMap;

use crossbeam_channel::Receiver;
use tokio::time::Instant;

pub enum GuiEvent {
    NewProxy,
    RequestDone(&'static str, Vec<u16>),
}

pub fn main(receiver: &Receiver<GuiEvent>) {
    let start_time = Instant::now();
    const UPDATE_INTERVAL: u64 = 60;
    println!("Statistic will be printed every {} seconds", UPDATE_INTERVAL);
    let mut enemies: Vec<&'static str> = vec![];
    let mut proxies = 0;
    let mut initial_success_requests = 0;
    let mut initial_failed_requests = 0;
    let mut latest_success_requests = 0;
    let mut latest_failed_requests = 0;
    let mut last_update_time = Instant::now();
    let mut fail_reasons = HashMap::new();

    loop {
        if let Ok(event) = receiver.try_recv() {
            match event {
                GuiEvent::NewProxy => {
                    proxies += 1;
                }
                GuiEvent::RequestDone(site, statuses) => {
                    if !enemies.contains(&site) {
                        enemies.push(site);
                    }

                    for status in statuses {
                        if !(400..=500).contains(&status) && status != 0 {
                            latest_success_requests += 1;
                        } else {
                            latest_failed_requests += 1;
                        }

                        let val = fail_reasons.entry(status).or_insert(0);
                        *val += 1;
                    }
                }
            }
        }

        if last_update_time.elapsed().as_secs() >= UPDATE_INTERVAL {
            let latest_total_requests = latest_success_requests + latest_failed_requests;
            let initial_total_requests = initial_success_requests + initial_failed_requests;
            let rpm = latest_total_requests - initial_total_requests;
            let success_rpm = latest_success_requests - initial_success_requests;
            let mut rate = (success_rpm as f32 / rpm as f32 * 100.0).round();
            let mut total_rate = (latest_success_requests as f32 / latest_total_requests as f32 * 100.0).round();

            if rate.is_nan() {
                rate = 0.0;
            }
            if total_rate.is_nan() {
                total_rate = 0.0;
            }

            let duration = start_time.elapsed();
            let seconds = duration.as_secs() % 60;
            let minutes = (duration.as_secs() / 60) % 60;
            let hours = (duration.as_secs() / 60) / 60;

            println!("----------------------------------");
            println!("{} {}:{}:{}", add_spaces("Working time:"), hours, minutes, seconds);
            println!("{} {}", add_spaces("Enemies:"), enemies.len());
            println!("{} {}", add_spaces("Proxies:"), proxies);
            println!("{} {}", add_spaces("Rpm:"), rpm);
            println!("{} {}", add_spaces("Success rpm:"), success_rpm);
            println!("{} {}%", add_spaces("Success rpm rate:"), rate);
            println!("{} {}", add_spaces("Total requests:"), latest_total_requests);
            println!("{} {}", add_spaces("Total success requests:"), latest_success_requests);
            println!("{} {}%", add_spaces("Total success rate:"), total_rate);

            let mut statuses = fail_reasons.keys().into_iter().collect::<Vec<&u16>>();
            statuses.sort_by(|a, b| {
                fail_reasons.get(b).unwrap().cmp(fail_reasons.get(a).unwrap())
            });
            println!("Statuses:");

            for status in statuses {
                let amount = *fail_reasons.get(status).unwrap();
                let percent = (amount as f32 / latest_total_requests as f32 * 100.0).round();
                let status_message = if *status == 0 { "  0".to_string() } else { status.to_string() };
                println!("  {status_message}: {percent}% ({amount})");
            }
            println!();

            last_update_time = Instant::now();
            initial_success_requests = latest_success_requests;
            initial_failed_requests = latest_failed_requests;
        }
    }
}

fn add_spaces(message: &str) -> String {
    let max_length = 25;

    [message, &" ".repeat(max_length - message.len())].concat()
}
