use std::time::{SystemTime, UNIX_EPOCH};

use once_cell::sync::Lazy;

use crate::config_get_threads_amount;

static ENEMIES: Lazy<Vec<&str>> = Lazy::new(|| {
    include_str!("../config/enemy.txt").split('\n').collect::<Vec<&str>>()
});

pub fn get_enemies() -> Vec<&'static str> {
    let minute = get_current_minute();
    let enemies_per_minute = ENEMIES.len() / 60;

    let mut start_enemy = minute * enemies_per_minute;
    let amount = config_get_threads_amount();

    if ENEMIES.len() - start_enemy < amount {
        start_enemy = ENEMIES.len() - start_enemy;
    }

    ENEMIES.iter()
        .skip(start_enemy)
        .take(amount)
        .copied()
        .collect()
}

fn get_current_minute() -> usize {
    let secs = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs();
    let days = secs / 60 / 60 / 24;
    let day_secs = days * 24 * 60 * 60;
    let hours = (secs - day_secs) / 60 / 60;
    let hour_secs = hours * 60 * 60;
    let minutes = (secs - day_secs - hour_secs) / 60;

    minutes as usize
}