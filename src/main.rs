extern crate core;

use std::{fs, thread};
use std::future::Future;
use std::io::Write;
use std::sync::RwLock;
use std::time::Duration;

use crossbeam_channel::{bounded, Receiver, Sender, unbounded};
use futures::future::join_all;
use once_cell::sync::Lazy;

use crate::config::{config_get_request_per_thread, config_get_threads_amount};
use crate::enemy::get_enemies;
use crate::enemy_thread_connection::{EnemyThreadConnection, EnemyThreadOutEvent};
use crate::gui::GuiEvent;
use crate::proxy::start_pulling_proxies;
use crate::requester::request;

mod requester;
mod enemy;
mod proxy;
mod config;
mod gui;
pub mod enemy_thread_connection;

static GUI_CONNECTION: Lazy<(Sender<GuiEvent>, Receiver<GuiEvent>)> = Lazy::new(unbounded);
static PROXIES: Lazy<RwLock<Vec<String>>> = Lazy::new(|| {
    RwLock::new(vec![])
});

fn main() {
    thread::spawn(move || {
        let thread_connections = create_load_threads();
        let mut enemies = get_enemies();
        let mut enemies_iter = enemies.into_iter();
        let (proxy_tx, proxy_rx) = bounded(50);
        start_pulling_proxies(proxy_tx);

        loop {
            pull_new_proxies(&GUI_CONNECTION.0, &proxy_rx);

            for (in_, out) in &thread_connections {
                while let Ok(event) = out.try_recv() {
                    match event {
                        EnemyThreadOutEvent::GetNewEnemy => {
                            let mut next_enemy = enemies_iter.next();

                            if next_enemy.is_none() {
                                enemies = get_enemies();
                                enemies_iter = enemies.into_iter();
                                next_enemy = enemies_iter.next();
                                pull_new_proxies(&GUI_CONNECTION.0, &proxy_rx);
                            }

                            let enemy = next_enemy.unwrap();

                            in_.send(enemy)
                                .expect("Cannot send enemy to thread");
                        }
                    }
                }
            }
        }
    });

    gui::main(&GUI_CONNECTION.1);
}

fn create_load_threads() -> Vec<(Sender<&'static str>, Receiver<EnemyThreadOutEvent>)> {
    let amount_of_threads = config_get_threads_amount();
    let mut txes = vec![];

    for _ in 0..amount_of_threads {
        let (in_tx, in_rx) = bounded(1);
        let (out_tx, out_rx) = bounded(1);
        let connection = EnemyThreadConnection::new(in_rx, out_tx, &GUI_CONNECTION.0);

        load_thread(connection);
        txes.push((in_tx, out_rx));
    }

    txes
}

fn load_thread(mut connection: EnemyThreadConnection) {
    thread::spawn(move || {
        run_async(async {
            loop {
                let enemy = connection.get_new_enemy();

                match enemy {
                    None => { break; }
                    Some(e) => {
                        load(e, &connection).await;
                    }
                }

                thread::sleep(Duration::from_millis(100));
            }
        })
    });
}

async fn load(enemy: &'static str, connection: &EnemyThreadConnection) {
    let chunk_size = config_get_request_per_thread();

    for chunk in PROXIES.read().unwrap().chunks(chunk_size) {
        join_all(
            chunk.iter()
                .map(|proxy| request(enemy, proxy, connection))
        ).await;
    }
}

pub fn run_async<F: Future>(future: F) -> F::Output {
    let rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap();

    rt.block_on(future)
}

fn pull_new_proxies(gui_tx: &Sender<GuiEvent>, proxy_rx: &Receiver<String>) {
    while let Ok(proxy) = proxy_rx.try_recv() {
        PROXIES.write().unwrap().push(proxy);
        gui_tx.send(GuiEvent::NewProxy)
            .expect("Cannot send gui event");
    }
}

pub fn log_to_file(text: &str) {
    let mut file = fs::OpenOptions::new()
        .create(true)
        .write(true)
        .append(true)
        .open("file.txt")
        .unwrap();

    let data = [text, "\n"].concat();

    file.write_all(data.as_bytes())
        .expect("Cannot log to file");
}
