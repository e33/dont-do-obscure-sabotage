use std::thread;
use std::time::Duration;

use crossbeam_channel::Sender;
use futures::future::join_all;
use reqwest::Response;

#[cfg(test)]
use test::Bencher;

use crate::config::{config_get_proxy_verifier, config_get_request_per_thread, config_get_timeout_secs};
use crate::requester::create_proxy;
use crate::run_async;

pub fn start_pulling_proxies(proxy_tx: Sender<String>) {
    thread::spawn(move || {
        run_async(async {
            // TODO endlessly pull unique proxies
            pull_proxies(proxy_tx).await;
        });
    });
}

async fn pull_proxies(tx: Sender<String>) {
    validate_and_send_proxies(get_static_proxies(), &tx).await;
    pull_github_lists_of_proxies(&tx).await;
    pull_free_proxies(&tx).await;
}

async fn validate_and_send_proxies(all_proxies: Vec<String>, tx: &Sender<String>) {
    let chunk_size = config_get_request_per_thread();

    for proxies_chunk in all_proxies.chunks(chunk_size) {
        let responses = join_all(
            proxies_chunk.iter()
                .map(|ip| validate_proxy(ip))
        ).await;

        for (i, res) in responses.into_iter().enumerate() {
            if let Ok(response) = res {
                let status = response.status().as_u16();

                if status == 200 {
                    let ip = proxies_chunk[i].clone();
                    tx.send(ip)
                        .expect("Cannot send proxies statistic");
                }
            }
        }
    }
}

async fn pull_free_proxies(tx: &Sender<String>) {
    let required_speed = config_get_timeout_secs() / 2 * 1000;
    let mut page = 1;

    loop {
        let url = format!("https://www.freeproxy.world/?type=&anonymity=&country=&speed={}&port=&page={}", required_speed, page);
        let result = reqwest::Client::builder()
            .timeout(Duration::from_secs(30))
            .build()
            .expect("Cannot build http client")
            .get(url)
            .send()
            .await;

        if let Ok(response) = result {
            let text = response.text().await
                .expect("Cannot read response");

            let ips = parse_ips(&text);
            if ips.is_empty() {
                break;
            }

            validate_and_send_proxies(ips, tx).await;
        }

        page += 1;
    }
}

fn parse_ips(text: &str) -> Vec<String> {
    let ip_matcher = "show-ip-div\">";
    let port_matcher = "?port=";

    text.match_indices(ip_matcher).into_iter()
        .map(|(ip_index, ..)| {
            let mut concatenation = vec![];
            let ip_start = ip_index + ip_matcher.len();
            for char in text[ip_start..].chars() {
                if char == '<' {
                    break;
                }

                concatenation.push(char);
            }

            concatenation.push(':');
            let (port_index, ..) = text[ip_start..].match_indices(port_matcher).into_iter().next().unwrap();
            let port_start = port_index + port_matcher.len();
            for char in text[ip_start + port_start..].chars() {
                if char == '"' {
                    break;
                }

                concatenation.push(char);
            }

            concatenation.into_iter().collect::<String>()
                .replace('\n', "")
                .replace(' ', "")
                .trim()
                .to_owned()
        })
        .collect()
}

fn get_static_proxies() -> Vec<String> {
    include_str!("../config/proxy.txt").split('\n')
        .map(|e| e.to_owned())
        .collect::<Vec<String>>()
}

async fn pull_github_lists_of_proxies(tx: &Sender<String>) {
    let proxy_source: Vec<&str> = vec![
        "https://raw.githubusercontent.com/hookzof/socks5_list/master/proxy.txt",
        "https://raw.githubusercontent.com/clarketm/proxy-list/master/proxy-list-raw.txt",
        "https://raw.githubusercontent.com/TheSpeedX/PROXY-List/master/http.txt",
    ];

    for source in proxy_source {
        let data = reqwest::Client::builder()
            .timeout(Duration::from_secs(30))
            .build()
            .expect("Cannot build http client")
            .get(source)
            .send()
            .await;

        if let Ok(proxies) = data {
            let text = proxies.text().await;

            if let Ok(t) = text {
                let p: Vec<String> = t
                    .split('\n')
                    .filter(|ip| !ip.is_empty())
                    .map(|ip| ip.to_string())
                    .collect();

                validate_and_send_proxies(p, tx).await;
            }
        }
    }
}

async fn validate_proxy(ip: &str) -> Result<Response, reqwest::Error> {
    let timeout = config_get_timeout_secs() / 2;
    let verifier = config_get_proxy_verifier();

    reqwest::Client::builder()
        .timeout(Duration::from_secs(timeout))
        .proxy(create_proxy(ip))
        .build()
        .expect("Cannot build http client")
        .get(verifier)
        .send()
        .await
}


#[test]
fn test_parse_ips() {
    let text = "<tr><td class=\"show-ip-div\">203.32.120.205</td><td><a href=\"/?port=80\">80</a></tr>".to_owned();
    let ips = parse_ips(&text);

    assert_eq!(ips, vec!["203.32.120.205:80".to_owned()]);
}

#[cfg(test)]
#[bench]
fn bench_parse_ips(b: &mut Bencher) {
    let text = "<tr><td class=\"show-ip-div\">203.32.120.205</td><td><a href=\"/?port=80\">80</a></tr>".to_owned();

    b.iter(|| {
        parse_ips(&text);
    })
}